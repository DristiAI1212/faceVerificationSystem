# faceVerificationSystem
Immediate tasks
- [x] all primary code completed 
- [ ]  testing
- [ ] create a notebook showcasing how it works
- [ ] write a detection script for making a new dataset with ids

Later tasks
- [ ] replace open cv face detection by writing another module
- [ ] store all the encodings in a database offline/online for fast data input 
